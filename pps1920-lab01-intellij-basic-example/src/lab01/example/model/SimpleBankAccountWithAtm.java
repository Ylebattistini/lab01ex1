package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm{

    private int atmfee=1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public final void depositWithAtm( final int usrID, final double amount){
        super.deposit(usrID, amount-this.atmfee);
    }

    @Override
    public final void withdrawAtm(final int usrID, final double amount){
        super.withdraw(usrID, amount+this.atmfee);
    }
}
