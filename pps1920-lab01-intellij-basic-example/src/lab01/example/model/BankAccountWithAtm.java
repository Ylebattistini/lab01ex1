package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount{

    void depositWithAtm(int usrID, double amount);

    void withdrawAtm(int usrID, double amount);
}
