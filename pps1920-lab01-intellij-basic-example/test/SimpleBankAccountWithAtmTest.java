import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountWithAtmTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccount;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Luca", "Bianchi", 1);
        bankAccount = new SimpleBankAccountWithAtm(accountHolder, 0);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }

    @Test
    void testDeposit() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWrongDeposit() {
        bankAccount.depositWithAtm(accountHolder.getId(), 200);
        bankAccount.depositWithAtm(2, 50);
        assertEquals(199, bankAccount.getBalance());
    }

    @Test
    void testWithdraw() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        bankAccount.withdrawAtm(accountHolder.getId(), 70);
        assertEquals(28, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdraw() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        bankAccount.withdrawAtm(2, 70);
        assertEquals(99, bankAccount.getBalance());
    }
}
