import lab01.tdd.CircularListImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularListImpl circulatList;

     @BeforeEach
        public void setUp() throws Exception{
         this.circulatList= new CircularListImpl();
     }

     @Test
    public void isInitallyEmpty(){
         assertTrue(this.circulatList.isEmpty());
     }

     @Test
    public void addElement(){
         this.circulatList.add(1);
         assertTrue(this.circulatList.size()==1);
     }

     @Test
     public void addMoreElement(){
         this.circulatList.add(4);
         this.circulatList.add(1);
         assertTrue(this.circulatList.size()==2);
     }

     @Test
    public void reset(){
         assertTrue(this.circulatList.isEmpty());
         this.circulatList.add(4);
         this.circulatList.add(1);
         this.circulatList.reset();
         assertTrue(this.circulatList.isEmpty());
     }

     @Test
    public void initiallyReset(){
         this.circulatList.reset();
         this.circulatList.add(1);
         assertFalse(this.circulatList.isEmpty());
     }


     @Test
    public void nextElement(){
         this.circulatList.add(4);
         this.circulatList.add(1);
         assertEquals(1,this.circulatList.next().get());
         assertEquals(4,this.circulatList.next().get());
     }


    @Test
    public void previousElement(){
        this.circulatList.add(4);
        this.circulatList.add(1);
        assertTrue(this.circulatList.size()==2);
        assertEquals(1,this.circulatList.next().get());
        assertEquals(4, this.circulatList.previous().get());
    }


    @Test
    public void nextStrategyEmpty(){
        assertEquals(Optional.empty(), this.circulatList.next(e -> e == 1));
    }


    @Test
    public void multipleStrategy(){
        this.circulatList.add(4);
        this.circulatList.add(8);
        this.circulatList.add(10);
        this.circulatList.add(11);
        assertTrue(this.circulatList.size()==4);
        assertEquals(8, this.circulatList.next(SelectStrategyRealImpl.multipleOfStrategy(4)).get());
    }

    @Test
    public void evenStrategy(){
        this.circulatList.add(2);
        this.circulatList.add(8);
        this.circulatList.add(4);
        this.circulatList.add(11);
        assertTrue(this.circulatList.size()==4);
        assertEquals(8, this.circulatList.next(SelectStrategyRealImpl.evenStrategy()).get());
    }

    @Test
    public void equalStrategy(){
        this.circulatList.add(4);
        this.circulatList.add(8);
        this.circulatList.add(10);
        this.circulatList.add(11);
        assertTrue(this.circulatList.size()==4);
        assertEquals(8, this.circulatList.next(SelectStrategyRealImpl.isEqualToStrategy(8)).get());
        assertEquals(10, this.circulatList.next(SelectStrategyRealImpl.isEqualToStrategy(10)).get());
        assertEquals(11, this.circulatList.next(SelectStrategyRealImpl.isEqualToStrategy(11)).get());
        assertEquals(4, this.circulatList.next(SelectStrategyRealImpl.isEqualToStrategy(4)).get());
        assertEquals(8, this.circulatList.next(SelectStrategyRealImpl.isEqualToStrategy(8)).get());
    }

}
