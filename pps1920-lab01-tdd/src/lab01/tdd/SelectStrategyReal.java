package lab01.tdd;

public interface SelectStrategyReal {

    SelectStrategy evenStrategy();
    SelectStrategy multipleOfStrategy(int number);
    SelectStrategy isEqualToStrategy(int number);
}
