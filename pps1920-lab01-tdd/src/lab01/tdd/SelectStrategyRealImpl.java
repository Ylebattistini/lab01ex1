package lab01.tdd;

public class SelectStrategyRealImpl {

    public static SelectStrategy evenStrategy() {
        return multipleOfStrategy(2);
    }


    public static SelectStrategy multipleOfStrategy(int number) {
        return value -> value % number ==0;
    }


    public static SelectStrategy isEqualToStrategy(int number) {
        return value -> value == number;
    }
}
