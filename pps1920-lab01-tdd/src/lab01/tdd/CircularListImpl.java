package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class CircularListImpl implements CircularList {

    public static final int MEMBER = 1;
    private List<Integer> list = new LinkedList<>();
    private int pointElement = 0;

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (this.pointElement < this.list.size()-MEMBER)
            this.pointElement++;
        else this.pointElement=0;
            return this.list.isEmpty() ? Optional.empty() : Optional.of(this.list.get(this.pointElement));
    }


    @Override
    public Optional<Integer> previous() {
        if (this.pointElement > 0) {
            this.pointElement--;
        } else {
            this.pointElement = this.list.size() - MEMBER;
        }
        return this.list.isEmpty() ? Optional.empty() : Optional.of(this.list.get(this.pointElement));
    }

    @Override
    public void reset() {
        this.list.clear();
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        return Stream.generate(this::next)
                .limit(this.size())
                .map(Optional::get)
                .filter(strategy::apply)
                .findFirst();
    }
}
